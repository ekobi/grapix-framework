defmodule Mix.Tasks.Gpx.New do
  @moduledoc """
  Implements a Mix task for generating a new Grapix API project.
  """

  @doc """

  ## Examples

        mix gpx.new my_api --include <api_module1> --include <api_module2>

        mix gpx.new my_api --ecto --path 'projects/foo/my_api'

        mix gpx.new my_api --demo

  """

  use Mix.Task
  @opt_parse_flags [ecto: :boolean, demo: :boolean, path: :string, include: :keep]
  @opt_parse_aliases [e: :ecto, d: :debug, i: :include, p: :path]
  @version Mix.Project.config()[:version]

  def run([args]) when args in ~w(-v --version) do
    Mix.shell().info("Grapix v#{@version}")
  end

  def run(argv) do
    case parse_opts(argv) do
      {_opts, []} ->
        Mix.Tasks.Help.run(["gpx.new"])

      {opts, [project_name | _]} ->
        generate(project_name, opts)
    end
  end

  defp parse_opts(argv) do
    case OptionParser.parse(argv, strict: @opt_parse_flags, aliases: @opt_parse_aliases) do
      {opts, argv, []} ->
        {opts, argv}

      {_opts, _argv, [switch | _]} ->
        Mix.raise("Invalid option: " <> switch_to_string(switch))
    end
  end

  defp switch_to_string({name, nil}), do: name
  defp switch_to_string({name, val}), do: name <> "=" <> val

  defp generate(project_name, opts) do
    path = Keyword.get(opts, :path, ".")
    Mix.shell().info("Generating into '#{path}' ...")
    GrapixTask.generate(:gen_template_grapix, project_name, ["--into", path])

    if opts[:ecto] do
      Mix.shell().info("Adding Ecto ...")
    end

    case Keyword.get_values(opts, :include) do
      [] -> nil
      api_modules -> Mix.shell().info("Adding API module(s) #{api_modules} ...")
    end
  end
end
