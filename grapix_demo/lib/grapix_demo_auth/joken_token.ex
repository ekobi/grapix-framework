defmodule GrapixDemoAuth.JokenToken do
  use Joken.Config, default_signer: nil

  add_hook(JokenJwks, strategy: GrapixDemoAuth.JokenStrategy)

  @impl true
  def token_config do
    default_claims(skip: [:aud, :iss])
    |> add_claim("roles", nil, &(&1 in ["admin", "user"]))
  end
end
