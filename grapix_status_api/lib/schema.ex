defmodule GrapixStatusApi.Schema do
  require Grapix.SchemaBuilder
  Grapix.SchemaBuilder.inject_schema_def([GrapixStatusApi])

  def middleware(middleware, _field, %Absinthe.Type.Object{identifier: identifier})
      when identifier in [:query, :subscription, :mutation] do
    middleware
  end

  def middleware(middleware, _field, _object) do
    middleware
  end
end
