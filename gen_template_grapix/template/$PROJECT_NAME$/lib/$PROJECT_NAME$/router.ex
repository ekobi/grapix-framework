defmodule <%= @project_name_camel_case %>.Router do
  import Plug.Conn
  use Plug.Router

  plug :match
  plug :dispatch

  forward "/graphiql",
    to: Absinthe.Plug.GraphiQL,
    init_opts: [schema: <%= @project_name_camel_case %>Api.Schema, json_codec: Jason]

  forward "/graphql",
    to: Absinthe.Plug,
    init_opts: [schema: <%= @project_name_camel_case %>Api.Schema, json_codec: Jason]

  match _ do
    send_resp(conn, 404, "Haipatikani, samahani.")
  end
end
