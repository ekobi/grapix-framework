# Grapix project generator

Implements Mix task for generating modular GraphQL API projects using
Absinthe and Phoenix. The generated API project comes with two OTP
Application components and a helper library:

- `lib/foo_project` is a stipped-down Phoenix application, wired up to
  serve your GraphQL API at `/graphql`. It's also set up to serve
  subscriptions over websockets, and to serve the GraphiQL playground
  at `/graphiql`.

- `lib/foo_project_api` is where you implement your API, using the
  Absinthe DSL to define your GraphQL types, fields, and
  resolvers. Ingress checking is implemented out of the box for the
  root fields only by default, but you can easily tune to suit your
  needs

  If you generated your project with Ecto, or if your business logic
  requires a stateful server, then this module will be a child app,
  wired into the main supervisor tree. 

- `lib/foo_project_auth` implements a JWT claims validation strategy,
  using Joken.

## Testing Support

The generated project includes a skeletal test framework for
validating GraphQL executable documents with parameters, and for
testing actual queries against the defined Schema and business logic
(the tests bypass the HTTP server, but they do not bypass ingress
checking fucntionality).

## Installation

If [available in Hex](https://hex.pm/docs/publish), the generator task
can be installed by with the archive installer:


```bash
    $ archive.install hex grapix_task
```
