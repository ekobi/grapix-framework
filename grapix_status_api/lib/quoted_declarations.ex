defmodule GrapixStatusApi.QuotedDeclarations do
  def types() do
    quote do
      enum :message_severity do
        value(:advisory, as: "advisory")
        value(:critical, as: "critical")
      end

      object :service_status_message do
        field(:timestamp, :datetime)
        field(:message, :string)
        field(:level, :message_severity)
      end

      object :service_status do
        field(:version, :string)
        field(:build, :integer)
        field(:env, :string)
        field(:version_string, :string)
        field(:minimum_client_build, :integer)
        field(:messages, list_of(:service_status_message))
      end
    end
  end

  def fields() do
    quote do
      alias GrapixStatusApi.Resolvers

      object :service_status_queries do
        @desc "Service configuration and advisory data"
        field :service_status, :service_status do
          resolve(&Resolvers.service_status_query/3)
        end
      end

      object :service_status_mutations do
        @desc "Add status advisory message"
        field :add_status_message, :service_status do
          arg(:message, non_null(:string))
          arg(:level, :message_severity)
          resolve(&Resolvers.add_status_message_mutation/3)
        end

        @desc "Trim status advisory messages"
        field :trim_status_messages, :service_status do
          arg(:residual, :integer)
          resolve(&Resolvers.trim_status_messages_mutation/3)
        end
      end

      object :service_status_subscriptions do
        @desc "Real-time service advisory data"
        field :service_status, :service_status do
          config(&Resolvers.service_status_subscription_config/2)

          trigger([:add_status_message, :trim_status_messages],
            topic: &Resolvers.service_status_subscription_topic/1
          )

          resolve(&Resolvers.service_status_subscription_resolver/3)
        end
      end
    end
  end

  def root_field_imports(:query) do
    quote do
      import_fields(:service_status_queries)
    end
  end

  def root_field_imports(:mutation) do
    quote do
      import_fields(:service_status_mutations)
    end
  end

  def root_field_imports(:subscription) do
    quote do
      import_fields(:service_status_subscriptions)
    end
  end

  def root_field_imports(_) do
    quote do
    end
  end
end
