defmodule <%= @project_name_camel_case %>Auth.Claims do
  @moduledoc """
  Decode and extract claims from ID or Access token in param
  (typically 'id_token' or 'access_token'). Accept either a plain
  binary string, or one wrapped in a list. Empty, missing, or
  invalid tokens all result in an empty claims map.
  """

  def from_token([]), do: from_token(nil)
  def from_token([token]), do: from_token(token)

  def from_token(token) when is_binary(token) or is_nil(token) do
    verify_and_validate(token)
    # |> IO.inspect(label: "Claims/from_token")
  end

  def from_token(_), do: %{}

  defp verify_and_validate(nil), do: %{}

  defp verify_and_validate(token) do
    case <%= @project_name_camel_case %>Auth.JokenToken.verify_and_validate(token) do
      {:ok, claims} -> claims
      _ -> %{}
    end
  end
end
