defmodule <%= @project_name_camel_case %>Api.QuotedDeclarations do
  def types() do
    quote do
    end
  end

  def fields() do
    quote do
      object :<%= @project_name %>_queries do
        @desc "The stuff that truly matters to <%= @project_name_camel_case %> ..."

        field :hello, :string do
          arg(:my_name_is, :string)

          resolve(fn _, args, _ ->
            {:ok, "Nafurahi kukuona, #{Map.get(args, :my_name_is, "mgeni")}!"}
          end)
        end
      end
    end
  end

  def root_field_imports(:query) do
    quote do
      import_fields(:<%= @project_name %>_queries)
    end
  end

  def root_field_imports(_) do
    quote do
    end
  end
end
