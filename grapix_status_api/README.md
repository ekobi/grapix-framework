# GrapixStatusApi

GrapixStatusApi is a self-contained GraphQL API demo module,
demonstrating how to decompose your API design into smaller,
independent building blocks that can be developed and tested
independently of each other.

It implements a `serviceStatus` query and subscription; and pair of
mutations to `addStatusMessage`s and `trimStatusMessages`. These docs
are [available online here](https://hexdocs.pm/grapix_status_api).

## Installation

Add `:grapix_status_api` as a dependency in a Grapix API project to
use it. If [available in Hex](https://hex.pm/packages/grapix_status_api), the
package can be installed by adding `grapix_status_api` to your list of
dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:grapix_status_api, "~> 0.1.0"}
  ]
end
```

By default, Mix will automatically start the `:grapix_status_api` OTP
application when you start your API server.

## Tests

There are a few canned GraphQL query documents defined in
`lib/graphql.ex`. Take a look at `test/grapix_status_api_test.exs` to
see how they are used to validate the query documents and arguments,
and also to execute live queries against the API schema. These tests
do not require a running HTTP server.

## Screenshots
### `addStatusMessage` mutation
![addStatusMessage](assets/images/demo-addStatusMessage-mutation.png)
### `serviceStatus` subscription
![serviceStatus](assets/images/demo-serviceStatus-subscription.png)
