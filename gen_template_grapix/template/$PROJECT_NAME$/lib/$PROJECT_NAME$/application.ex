defmodule <%= @project_name_camel_case %>.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      <%= @project_name_camel_case %>.Endpoint,
      {Absinthe.Subscription, [<%= @project_name_camel_case %>.Endpoint]}
    ]

    opts = [strategy: :one_for_one, name: <%= @project_name_camel_case %>.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    <%= @project_name_camel_case %>.Endpoint.config_change(changed, removed)
    :ok
  end
end
