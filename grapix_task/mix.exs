defmodule GrapixTask.MixProject do
  use Mix.Project

  @version "0.2.1"
  @name :grapix_task

  @maintainers ["Kobi Eshun <gitlab@e-kobi.com>"]
  @gitlab      "https://gitlab.com/ekobi/grapix-framework.git"
  @homepage    "https://ekobi.gitlab.io/grapix-framework"

  @description """
  Mix task to generate a modular GraphQL API project using Absinthe and Phoenix.
  """

  def project do
    [
      app: @name,
      version: @version,
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: [extras: ["README.md"]],
      package: package(),
      source_url: @gitlab,
      homepage_url: @homepage
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp package do
    [
      description: @description,
      name:        @name,
      files:       ["lib", "mix.exs", "README.md", "LICENSE.md"],
      maintainers: @maintainers,
      licenses:    ["Apache 2.0 (see LICENSE.md for details.)"],
      links:       %{
        "Homepage" => @homepage,
        "GitLab" => @gitlab
      },
    ]
  end

  defp deps do
    [
      { :ex_doc, "~>0.21.3",  only: [:dev, :test] },
      {:private, "~> 0.1.1"}
    ]
  end
end
