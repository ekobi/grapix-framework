defmodule <%= @project_name_camel_case %>Auth.JokenToken do
  use Joken.Config, default_signer: nil

  add_hook(JokenJwks, strategy: <%= @project_name_camel_case %>Auth.JokenStrategy)

  @impl true
  def token_config do
    default_claims(skip: [:aud, :iss])
    |> add_claim("roles", nil, &(&1 in ["admin", "user"]))
  end
end
