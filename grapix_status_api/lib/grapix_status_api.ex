defmodule GrapixStatusApi do
  require Logger
  use Agent

  def start_link(config_info \\ []) do
    default_config_info = [
      version: "0.1",
      build: 1,
      env: "dev",
      minimum_client_build: 1,
      env: "#{:net_adm.localhost()}"
    ]

    status_info = Keyword.merge(default_config_info, config_info) |> Enum.into(%{})

    %{env: env, version: version, build: build} = status_info

    status_info =
      Map.merge(
        status_info,
        %{
          :messages => [],
          :version_string =>
            "#{env}-#{version}-#{String.pad_leading(Integer.to_string(build), 3, "0")}"
        }
      )
      |> IO.inspect(label: "[service_status/start_link]")

    Agent.start_link(fn -> status_info end, name: __MODULE__)
  end

  def post_message(message, level \\ "advisory") do
    stamped_message = %{
      :message => message,
      :timestamp => Elixir.DateTime.utc_now(),
      :level => level
    }

    Agent.update(__MODULE__, fn status_info ->
      %{status_info | :messages => [stamped_message | status_info[:messages]]}
    end)

    info()
  end

  def trim_messages(residual \\ 0) do
    Agent.update(__MODULE__, fn status_info ->
      %{status_info | :messages => Enum.slice(status_info[:messages], 0, max(0, residual))}
    end)

    info()
  end

  def info() do
    # Agent.get(__MODULE__, fn status_info -> status_info |> IO.inspect(label: "[service_status/info]") end)
    Agent.get(__MODULE__, fn status_info -> status_info end)
  end
end
