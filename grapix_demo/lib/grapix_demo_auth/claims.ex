defmodule GrapixDemoAuth.Claims do
  @moduledoc """
  Decode and extract claims from ID or Access token in param
  (typically 'id_token' or 'access_token'). Implements Plug behavior accessed by
  endpoint, and also utility function used by user_socket.
  """

  def from_token([]), do: from_token(nil)
  def from_token([token]), do: from_token(token)

  def from_token(token) when is_binary(token) or is_nil(token) do
    verify_and_validate(token)
    # |> IO.inspect(label: "Claims/from_token")
  end

  def from_token(_), do: %{}

  defp verify_and_validate(nil), do: %{}

  defp verify_and_validate(token) do
    case GrapixDemoAuth.JokenToken.verify_and_validate(token) do
      {:ok, claims} -> claims
      _ -> %{}
    end
  end
end
