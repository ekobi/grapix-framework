defmodule <%= @project_name_camel_case %>.UserSocket do
  use Phoenix.Socket
  use Absinthe.Phoenix.Socket, schema: <%= @project_name_camel_case %>Api.Schema

  def connect(params, socket) do
    {:ok,
     Absinthe.Phoenix.Socket.put_options(socket,
       context: <%= @project_name_camel_case %>.Context.context_from_params(params)
     )}
  end

  def id(_socket), do: nil
end
