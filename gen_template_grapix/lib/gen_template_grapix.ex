defmodule GenTemplateGrapix do

  @moduledoc File.read!(Path.join([__DIR__, "../README.md"]))
  
  use MixTemplates,
    name:       :gen_template_grapix,
    short_desc: "Template for modular GrpahQL API projects",
    source_dir: "../template"


end
