defmodule <%= @project_name_camel_case %>Api.Schema do
  require Grapix.SchemaBuilder

  Grapix.SchemaBuilder.inject_schema_def(
    Application.compile_env(:<%= @project_name %>, [Schema, :api_modules], [<%= @project_name_camel_case %>Api])
  )

  def middleware(middleware, _field, %Absinthe.Type.Object{identifier: identifier})
      when identifier in [:query, :subscription, :mutation] do
    [<%= @project_name_camel_case %>Api.IngressCheck | middleware]
  end

  def middleware(middleware, _field, _object) do
    middleware
  end
end
