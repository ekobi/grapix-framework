defmodule GrapixDemo.Endpoint do
  use Phoenix.Endpoint, otp_app: :grapix_demo
  use Absinthe.Phoenix.Endpoint

  socket "/graphql", GrapixDemo.UserSocket,
    websocket: true,
    longpoll: false

  socket "/graphql-ws", GrapixDemo.UserSocket,
    websocket: [
      subprotocols: ["graphql-ws"],
      serializer: [{Grapix.GraphqlWS.V1.JSONSerializer, ">= 1.0.0"}]
    ],
    longpoll: false

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Jason

  plug Plug.MethodOverride
  plug Plug.Head

  plug GrapixDemo.Context

  plug GrapixDemo.Router,
    schema: GrapixDemoApi.Schema

end
