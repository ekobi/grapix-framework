defmodule GrapixStatusApi.Graphql do
  @moduledoc """
  Defines canned GraphQL query documents to facilitate testing and scripting.
  """
  alias Grapix.QuerySpec

  @messageFieldsFragment """
  fragment MessageFields on ServiceStatusMessage {
    message
    timestamp
    level
  }
  """

  @serviceStatusQuery """
    query ServiceStatusQuery{
      serviceStatus {
        build
        env
        minimumClientBuild
        version
        versionString
        messages { ... MessageFields }
      }
    }
  """

  @addStatusMessageMutation """
    mutation AddStatusMessageMutation($message: String!, $level: MessageSeverity) {
      addStatusMessage(message: $message, level: $level) {
        build
        env
        minimumClientBuild
        version
        versionString
        messages { ... MessageFields }
      }
    }
  """

  @trimStatusMessagesMutation """
    mutation TrimStatusMessagesMutation($residual: Int) {
      trimStatusMessages(residual: $residual) {
        build
        env
        minimumClientBuild
        version
        versionString
        messages { ... MessageFields }
      }
    }
  """

  def service_status_query_spec() do
    %QuerySpec{
      schema: GrapixStatusApi.Schema,
      op_name: "ServiceStatusQuery",
      selection: "serviceStatus",
      doc: @messageFieldsFragment <> @serviceStatusQuery
    }
  end

  def add_status_message_mutation_spec() do
    %QuerySpec{
      schema: GrapixStatusApi.Schema,
      op_name: "AddStatusMessageMutation",
      selection: "addStatusMessage",
      doc: @messageFieldsFragment <> @addStatusMessageMutation
    }
  end

  def trim_status_messages_mutation_spec() do
    %QuerySpec{
      schema: GrapixStatusApi.Schema,
      op_name: "TrimStatusMessagesMutation",
      selection: "trimStatusMessages",
      doc: @messageFieldsFragment <> @trimStatusMessagesMutation
    }
  end
end
