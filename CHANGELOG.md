# Changelog
All notable changes to the
[Grapix Framework](https://gitlab.com/ekobi/grapix-framework) will be documented in this file. The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2020-07-20
### Added
- Experimental support for the [`graphql-ws`](https://github.com/apollographql/subscriptions-transport-ws/blob/master/PROTOCOL.md) WebSockets subprotocol, mounted by default at `ws://example.com:/graphql-ws`. (Only the Absinthe/Phoenix websocket protocol is fully supported at present, mounted at `ws://example.com/graphql`).

### Changed
- Upgrade to Absinthe 1.5 and Phoenix 1.5
- Root query names in the composed API schema are changed from `RootSubscriptionType`/`RootQueryType`/`RootMutationType` to `Subscription`/`Query`/`Mutation`, which seems to be more standard. This works better with downstream tools that automatically generate types and query wrappers via schema introspection (*e.g.* [mst-gql](https://github.com/mobxjs/mst-gql))

## [0.1.2] - 2020-03-03
### Changed
- Bug fixes and cleanup

## [0.1.0] - 2020-02-24
### Added
- Initial release
