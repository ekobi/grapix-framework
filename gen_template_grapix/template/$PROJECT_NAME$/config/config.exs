import Config

# Configures the endpoint
config :<%= @project_name %>, <%= @project_name_camel_case %>.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ETl0Fea6oFjhVzKQmrXWvmNLTzTaI7nY+Q/FA6YxxCgm908m/j7K7TXfRQVWabgq",
  pubsub: [name: <%= @project_name_camel_case %>.PubSub, adapter: Phoenix.PubSub.PG2]

config :<%= @project_name %>, Context, claims_parameter: "id_token"

config :<%= @project_name %>, Schema, api_modules: [<%= @project_name_camel_case %>Api]

config :phoenix, :json_library, Jason

# Joken and friends
config :joken_jwks, <%= @project_name_camel_case %>Auth.JokenStrategy,
  jwks_url: "https://example.com/.well-known/jwks.json",
  time_interval: 4_000_000

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
