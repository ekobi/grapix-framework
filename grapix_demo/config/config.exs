import Config

# Configures the endpoint
config :grapix_demo, GrapixDemo.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ETl0Fea6oFjhVzKQmrXWvmNLTzTaI7nY+Q/FA6YxxCgm908m/j7K7TXfRQVWabgq",
  pubsub: [name: GrapixDemo.PubSub, adapter: Phoenix.PubSub.PG2]

config :grapix_demo, Context, claims_parameter: "id_token"

config :grapix_demo, Schema, api_modules: [GrapixDemoApi, GrapixStatusApi]

config :phoenix, :json_library, Jason

# Joken and friends
config :joken_jwks, GrapixDemoAuth.JokenStrategy,
  jwks_url: "https://chipi-dev.auth0.com/.well-known/jwks.json",
  time_interval: 4_000_000

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
