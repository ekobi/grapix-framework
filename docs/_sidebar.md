- Introduction

  - [What](README.md#grapix-grape-ix)
  - [Quick Start](README.md#getting-started)
  - [Trying The Demo API](README.md#trying-the-external-demo-api-module)
  - [GraphQL Subscriptions](README.md#graphql-subscriptions)
    - [@absinthe/socket-apollo-link](README.md#absinthesocket-apollo-link)
    - [Apollo Client Support](README.md#apollo-client-support-experimental)
    - [MobX State Tree/ GQL](README.md#mst-gql-integration)
  - [Development Pipeline](README.md#pipeline)

- Modules
  - [Demo Module (Status API)](grapix_status_api.md)
  - [Demo Application](grapix_demo.md)
  - [Core](grapix.md)
  - [Mix Task](grapix_task.md)
  - [Generator](gen_template_grapix.md)

- Changelog
  - [Changelog](CHANGELOG.md)
