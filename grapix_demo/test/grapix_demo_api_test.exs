defmodule GrapixDemoApiTest do
  use ExUnit.Case

  require Logger
  alias Grapix.GraphqlOps
  alias GrapixDemoApi.Graphql

  use ExUnit.Case, async: false

  describe "Validate GraphQL execution document specs used for testing" do
    # setup []

    test "Validate GraphQL query documents" do
      {status, errors} = GraphqlOps.validate_query_spec(Graphql.hello_query_spec())
      assert :ok == status, "Validation errors: #{inspect(errors, pretty: true)}"
    end
  end

  describe "Test hospitality." do
    test "Should respond graciously to anonymous greeter" do
      {:ok, query_result} =
        GraphqlOps.execute(
          Graphql.hello_query_spec(),
          vars: %{},
          context: %{authenticated_claims: %{}}
        )

      refute nil == query_result
      assert query_result == "Nafurahi kukuona, mgeni!"
    end

    test "Should respond with personalized greeting" do
      {:ok, query_result} =
        GraphqlOps.execute(
          Graphql.hello_query_spec(),
          vars: %{my_name_is: "DJ ExUnit"},
          context: %{authenticated_claims: %{}}
        )

      refute nil == query_result
      assert query_result == "Nafurahi kukuona, DJ ExUnit!"
    end
  end
end
