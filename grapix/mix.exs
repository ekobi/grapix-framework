defmodule Grapix.MixProject do
  use Mix.Project

  @maintainers ["Kobi Eshun <gitlab@e-kobi.com>"]
  @gitlab "https://gitlab.com/ekobi/grapix-framework.git"
  @homepage "https://ekobi.gitlab.io/grapix-framework"

  @description """
  Helpers for building modular GraphQL API projects using Absinthe and Phoenix.
  """

  def project do
    [
      app: :grapix,
      version: "0.2.1",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      name: "Grapix",
      source_url: "https://gitlab.com/ekobi/grapix-framework.git",
      docs: [extras: ["README.md"]],
      package: package(),
      source_url: @gitlab,
      homepage_url: @homepage
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:absinthe, "~> 1.5"},
      {:morphix, "~> 0.8"},
      {:dialyxir, "~>0.5.1", only: [:dev], runtime: false},
      {:ex_doc, "~>0.21.3", only: [:dev, :test]},
      {:phoenix, "~> 1.5"},
      {:puid, "~> 1.0"}
    ]
  end

  defp package do
    [
      description: @description,
      files: ["lib", "mix.exs", "README.md", "LICENSE.md"],
      maintainers: @maintainers,
      licenses: ["Apache 2.0 (see LICENSE.md for details.)"],
      links: %{
        "Homepage" => @homepage,
        "GitLab" => @gitlab
      }
    ]
  end
end
