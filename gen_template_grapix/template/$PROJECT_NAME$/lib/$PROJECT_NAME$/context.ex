defmodule <%= @project_name_camel_case %>.Context do
  @moduledoc """
  Implements `Context` Plug used in `endpoint`, and also helper for adding context to `user_socket`.

  In either case, it creates context containing an `authenticated_claims` key/value pair, where
  `authenticated_claims` is itself a (possibly-empty) map.

  We assume the claims will be derived from a header parameter, defined at compile-time via the
  `[:<%= @project_name %>, [Context, :claims_parameter]]` environment key. See the `<%= @project_name_camel_case %>Auth` for
  the implementation of claim extraction, and also the hook for implementing API ingress checking
  using the claims.
  """

  @behaviour Plug

  import Plug.Conn

  alias <%= @project_name_camel_case %>Auth.Claims

  @claims_parameter Application.compile_env!(:<%= @project_name %>, [Context, :claims_parameter])
  def init(opts), do: opts

  def call(conn, _) do
    claims =
      conn
      |> fetch_query_params(conn)
      |> get_req_header(@claims_parameter)
      # |> IO.inspect(label: "#{__MODULE__} #{@claims_parameter} from header")
      |> token_from_header
      |> Claims.from_token()

    Absinthe.Plug.put_options(conn, context: %{authenticated_claims: claims})
  end

  def context_from_params(params) do
    claims =
      Map.get(params, @claims_parameter, nil)
      |> Claims.from_token()

    %{authenticated_claims: claims}
  end

  defp token_from_header([token | _]), do: token_from_header(token)
  defp token_from_header(token) when is_binary(token) or is_nil(token), do: token
  defp token_from_header(_), do: nil
end
