# Grapix <small>[GRAPE-ix]</small>

> A framework for building modular GraphQL APIs with Phoenix and Absinthe

[GitLab](https://gitlab.com/ekobi/grapix-framework.git)
[Get Started](#getting-started)
