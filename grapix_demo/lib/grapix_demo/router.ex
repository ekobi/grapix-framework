defmodule GrapixDemo.Router do
  import Plug.Conn
  use Plug.Router

  plug :match
  plug :dispatch

  forward "/graphiql",
    to: Absinthe.Plug.GraphiQL,
    init_opts: [schema: GrapixDemoApi.Schema, json_codec: Jason]

  forward "/graphql",
    to: Absinthe.Plug,
    init_opts: [schema: GrapixDemoApi.Schema, json_codec: Jason]

  forward "/graphql-ws",
    to: Absinthe.Plug,
    init_opts: [schema: GrapixDemoApi.Schema, json_codec: Jason]

  match _ do
    send_resp(conn, 404, "Haipatikani, samahani.")
  end
end
