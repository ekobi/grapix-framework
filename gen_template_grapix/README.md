# GenTemplateGrapix

A MixTemplate-compatible template for generating modular GraphQL API
projects. Although you can use it directly with `mix gen`, you should
consider using `mix gpx.new`, which wraps use of this and other
templates into a simpler command-line tool.

## Installation

You probably will need to install the MixTemplates and MixGenerator:

    $ mix archive.install hex mix_templates
    $ mix archive.install hex mix_generator

Then install this template locally to your machine:

    $ mix template.install hex gen_template_grapix

Finally, generate a Grapix project:

    $ mix gen gen_template_grapix my_project

## License

Apache 2.0. See [LICENSE.md](LICENSE.md) for details.

----
Created:  2020-02-19Z
