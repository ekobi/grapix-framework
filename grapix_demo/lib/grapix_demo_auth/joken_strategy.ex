defmodule GrapixDemoAuth.JokenStrategy do
  use JokenJwks.DefaultStrategyTemplate

  def start_link() do
    opts = Application.get_env(:joken_jwks, __MODULE__)
    start_link(opts)
  end
end
