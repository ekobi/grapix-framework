defmodule GenTemplateGrapix.Mixfile do
  use Mix.Project

  @name    :gen_template_grapix
  @version "0.2.1"

  @deps [
    { :mix_templates,  "~>0.2.1",  app: false },
    { :ex_doc,         "~>0.21.3",  only: [:dev, :test] },
  ]

  @maintainers ["Kobi Eshun <gitlab@e-kobi.com>"]
  @gitlab      "https://gitlab.com/ekobi/grapix-framework.git"
  @homepage    "https://ekobi.gitlab.io/grapix-framework"

  @description """
  Generates a modular GraphQL API project using Absinthe and Phoenix.
  """

  # ------------------------------------------------------------

  def project do
    in_production = Mix.env == :prod
    [
      app:     @name,
      version: @version,
      deps:    @deps,
      elixir:  "~> 1.10",
      package: package(),
      description:     @description,
      build_embedded:  in_production,
      start_permanent: in_production,
      source_url: @gitlab,
      homepage_url: @homepage
    ]
  end

  defp package do
    [
      name:        @name,
      files:       ["lib", "mix.exs", "README.md", "LICENSE.md", "template"],
      maintainers: @maintainers,
      licenses:    ["Apache 2.0 (see LICENSE.md for details.)"],
      links:       %{
        "GitLab" => @gitlab,
        "Homepage" => @homepage
      },
    ]
  end

end
