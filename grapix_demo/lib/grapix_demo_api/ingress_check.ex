defmodule GrapixDemoApi.IngressCheck do
  @behaviour Absinthe.Middleware

  require Logger

  def call(resolution, _config) do
    case resolution.context do
      %{authenticated_claims: %{}} ->
        Logger.warn("#{__MODULE__} FIXME!! Ingress checking disabled!")
        resolution

      _ ->
        # FIXME: implement application-specific authorization checks here
        resolution
        |> Absinthe.Resolution.put_result({:error, "not authorized"})
    end
  end
end
