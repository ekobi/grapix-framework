defmodule <%= @project_name_camel_case %>Auth.JokenStrategy do
  use JokenJwks.DefaultStrategyTemplate

  def start_link() do
    opts = Application.get_env(:joken_jwks, __MODULE__)
    start_link(opts)
  end
end
