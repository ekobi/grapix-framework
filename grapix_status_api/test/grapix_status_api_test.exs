defmodule GrapixStatusApiTest do
  use ExUnit.Case

  require Logger
  alias Grapix.GraphqlOps
  alias GrapixStatusApi.Graphql

  use ExUnit.Case, async: false

  describe "Validate GraphQL execution document specs used for testing" do
    # setup []

    test "Validate status_messages GraphQL documents" do
      {status, errors} = GraphqlOps.validate_query_spec(Graphql.service_status_query_spec())
      assert :ok == status, "Validation errors: #{inspect(errors, pretty: true)}"
    end
  end

  describe "Test Service Status functionality" do
    setup [:initialize_status_messages]

    test "Should verify API reports service status" do
      {:ok, query_result} = GraphqlOps.execute(Graphql.service_status_query_spec())
      refute nil == query_result

      status_messages = get_in(query_result, ["messages"])
      assert 3 <= length(status_messages)
    end

    test "Should add status advisory message to head of the list with default ADVISORY level" do
      test_message = random_message("Hello, Ognam!")

      {:ok, mutation_result} =
        GraphqlOps.execute(
          Graphql.add_status_message_mutation_spec(),
          vars: %{message: test_message}
        )

      status_messages = get_in(mutation_result, ["messages"])
      assert test_message == hd(status_messages)["message"]
      assert "ADVISORY" == hd(status_messages)["level"]
    end

    test "Should add status CRITICAL advisory message to head of the list" do
      test_message = random_message("Hello, Spud!")

      {:ok, mutation_result} =
        GraphqlOps.execute(
          Graphql.add_status_message_mutation_spec(),
          vars: %{message: test_message, level: "CRITICAL"}
        )

      status_messages = get_in(mutation_result, ["messages"])
      assert test_message == hd(status_messages)["message"]
      assert "CRITICAL" == hd(status_messages)["level"]
    end

    test "Should trim advisory message list" do
      {status, mutation_result} =
        GraphqlOps.execute(
          Graphql.trim_status_messages_mutation_spec(),
          vars: %{residual: 2}
        )

      assert status == :ok

      status_messages = get_in(mutation_result, ["messages"])
      assert 2 == length(status_messages)
    end

    defp random_message(message_body), do: "[#{:rand.uniform(1000)}] #{message_body}"

    defp initialize_status_messages(_context) do
      Stream.repeatedly(fn ->
        GraphqlOps.execute(
          Graphql.add_status_message_mutation_spec(),
          vars: %{message: random_message("A useless announcement"), level: "ADVISORY"}
        )
      end)
      |> Enum.take(3)
    end
  end
end
