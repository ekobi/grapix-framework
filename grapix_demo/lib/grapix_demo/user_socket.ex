defmodule GrapixDemo.UserSocket do
  use Phoenix.Socket
  use Absinthe.Phoenix.Socket, schema: GrapixDemoApi.Schema

  def connect(params, socket) do
    socket |> IO.inspect(label: "[#{__MODULE__}/connect] socket")
    {:ok,
     Absinthe.Phoenix.Socket.put_options(socket,
       context: GrapixDemo.Context.context_from_params(params)
     )}
  end

  def id(_socket), do: nil

end
