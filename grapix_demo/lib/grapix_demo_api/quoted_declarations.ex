defmodule GrapixDemoApi.QuotedDeclarations do
  def types() do
    quote do
    end
  end

  def fields() do
    quote do
      object :grapix_demo_queries do
        @desc "The stuff that truly matters to GrapixDemo ..."

        field :hello, :string do
          arg(:my_name_is, :string)

          resolve(fn _, args, _ ->
            {:ok, "Nafurahi kukuona, #{Map.get(args, :my_name_is, "mgeni")}!"}
          end)
        end
      end
    end
  end

  def root_field_imports(:query) do
    quote do
      import_fields(:grapix_demo_queries)
    end
  end

  def root_field_imports(_) do
    quote do
    end
  end
end
