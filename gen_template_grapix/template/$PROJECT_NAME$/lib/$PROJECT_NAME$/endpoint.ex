defmodule <%= @project_name_camel_case %>.Endpoint do
  use Phoenix.Endpoint, otp_app: :<%= @project_name %>
  use Absinthe.Phoenix.Endpoint

  socket "/graphql", <%= @project_name_camel_case %>.UserSocket,
    websocket: true,
    longpoll: false

  socket "/graphql-ws", <%= @project_name_camel_case %>.UserSocket,
    websocket: [
      subprotocols: ["graphql-ws"],
      serializer: [{Grapix.GraphqlWS.V1.JSONSerializer, ">= 1.0.0"}]
    ],
    longpoll: false

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Jason

  plug Plug.MethodOverride
  plug Plug.Head

  plug <%= @project_name_camel_case %>.Context

  plug <%= @project_name_camel_case %>.Router,
    schema: <%= @project_name_camel_case %>Api.Schema
end
