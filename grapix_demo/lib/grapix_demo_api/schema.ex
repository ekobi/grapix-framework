defmodule GrapixDemoApi.Schema do
  require Grapix.SchemaBuilder

  Grapix.SchemaBuilder.inject_schema_def(
    Application.compile_env(:grapix_demo, [Schema, :api_modules], [GrapixDemoApi])
  )

  def middleware(middleware, _field, %Absinthe.Type.Object{identifier: identifier})
      when identifier in [:query, :subscription, :mutation] do
    [GrapixDemoApi.IngressCheck | middleware]
  end

  def middleware(middleware, _field, _object) do
    middleware
  end
end
