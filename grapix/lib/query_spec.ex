defmodule Grapix.QuerySpec do
  @enforce_keys [:doc, :selection, :schema]
  defstruct [:doc, :op_name, :selection, :schema]

  @type t :: %__MODULE__{
          op_name: String.t(),
          selection: String.t(),
          doc: String.t(),
          schema: module
        }
end
