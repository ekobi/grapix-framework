# Grapix

This utility library implements a few helper modules for the Grapix
framework:

- `Grapix.ApiModule` defines behavior for a plug-in API module
- `Grapix.GraphqlOps` exports utility functions for manipulating
  GraphQL documents
- `Grapix.QuerySpec` defines an encapsulating structure for GraphQL
  executable documents
- `Grapix.SchemaBuilder` provides a macro to programmatically assemble
  an Absinthe schema from multiple API modules, at compile time

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `grapix` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:grapix, "~> 0.2"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/grapix](https://hexdocs.pm/grapix).

