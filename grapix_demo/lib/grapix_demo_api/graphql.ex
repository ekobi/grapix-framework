defmodule GrapixDemoApi.Graphql do
  @moduledoc """
  Defines canned GraphQL query documents to facilitate testing and scripting.
  """
  alias Grapix.QuerySpec

  @hello_query """
    query HelloQuery($my_name_is: String){
      hello(my_name_is: $my_name_is)
    }
  """
  def hello_query_spec() do
    %QuerySpec{
      schema: GrapixDemoApi.Schema,
      op_name: "HelloQuery",
      selection: "hello",
      doc: @hello_query
    }
  end
end
