defmodule GrapixStatusApi.MixProject do
  use Mix.Project

  @version "0.2.1"
  @name :grapix_status_api

  @maintainers ["Kobi Eshun <gitlab@e-kobi.com>"]
  @gitlab      "https://gitlab.com/ekobi/grapix-framework.git"
  @homepage    "https://ekobi.gitlab.io/grapix-framework"

  @description """
  GrapixStatusApi is self-contained GraphQL API demo module. It implements a `serviceStatus` query and subscription; and pair of mutations to add and trim status messages. Add :grapix_status_api as a dependency in a Grapix API project to use it.
  """

  def project do
    [
      app: @name,
      version: @version,
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: [extras: ["README.md"]],
      package: package(),
      aliases: aliases(),
      source_url: @gitlab,
      homepage_url: @homepage
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {GrapixStatusApi.Application, []}
    ]
  end

  defp deps do
    [
      {:absinthe, "~> 1.5"},
      {:grapix, "~> 0.2.1"},
      {:ex_doc, "~>0.21.3", only: [:dev, :test]}
    ]
  end

  defp package do
    [
      description: @description,
      name: @name,
      files: ["lib", "mix.exs", "README.md", "LICENSE.md"],
      maintainers: @maintainers,
      licenses: ["Apache 2.0 (see LICENSE.md for details.)"],
      links: %{
        "GitLab" => @gitlab,
        "Homepage" => @homepage
      }
    ]
  end

  defp aliases do
    [docs: ["docs", &copy_images/1]]
  end

  defp copy_images(_) do
    Path.wildcard("assets/**/.DS_Store", match_dot: true)
    |> IO.inspect(label: "pruning macOS turdlets")
    |> Enum.each(fn ds -> File.rm!(ds) end)

    File.cp_r("assets", "doc/assets", fn source, destination ->
      IO.gets("Overwriting #{destination} by #{source}. Type y to confirm. ") == "y\n"
    end)
  end
end
