defmodule Grapix.ApiModule do
  @moduledoc """
  Defines behaviors for plug-in API modules
  """

  @callback query_fields() :: atom()
  @callback mutation_fields() :: atom()
  @callback subscription_fields() :: atom()
  @callback app() :: [app: atom(), arg_hint: String.t()]
end
