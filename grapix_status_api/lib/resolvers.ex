defmodule GrapixStatusApi.Resolvers do
  def service_status_query(_, _, _) do
    {:ok, GrapixStatusApi.info()}
  end

  def add_status_message_mutation(_, %{message: message, level: level}, _context) do
    {:ok, GrapixStatusApi.post_message(message, level)}
  end

  def trim_status_messages_mutation(_, args, _context) do
    {:ok, GrapixStatusApi.trim_messages(args[:residual] || 0)}
  end

  def service_status_subscription_config(_args, %{context: context}) do
    case context do
      %{email: _email} ->
        {:ok, topic: :service_status_updates}

      _ ->
        {:ok, topic: :service_status_updates}

        # TODO make this okay only in DEV mode
        # _ -> {:error, "unauthorized"}
    end
  end

  def service_status_subscription_resolver(status, _args, _content) do
    {:ok, status}
  end

  def service_status_subscription_topic(_news) do
    :service_status_updates
  end
end
