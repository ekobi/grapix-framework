# Grapix \[GRAPE-ix\]

Grapix is a framework for building modular GraphQL APIs with Phoenix and Absinthe. [Follow this link](https://ekobi.gitlab.io/grapix-framework) for an expanded version of this documentation.

The first objective is to provide, out of the box, everything an API developer needs to start implementing their API. Generated projects include a stripped-down Phoenix Endpoint with websocket-based support for subscriptins; GraphQL executable document validation test support; API query test support; JWT claim extraction and validation support; ingress checking for root-level query fields; and a GraphiQL playground.

The second objective (the "modular" part) is to make it easy to decompose your API design into smaller building blocks that can be developed and tested independently, and that can be reused elsewhere. An API module can encapsulate any servers it needs, including a DB. Your generated project includes one such API module to get you started; you can augment your API with additional API modules either within your project, or from a completely separate project.

## Getting started

[`:grapix_task`](https://hexdocs.pm/grapix_task/readme.html#content) implements the `gpx.new` Mix generator task. To install and try it out:

```bash
    $ mix archive.install hex mix_templates
    $ mix archive.install hex mix_generator
    $ mix template.install hex gen_template_grapix
    $ mix archive.install hex grapix_task
    $ mix gpx.new my_api_project
    $ cd my_api_project
    $ mix deps.get
    $ mix test
    $ mix phx.server
    $ open http://localhost:4000/graphiql
```

## Trying the external demo API module

[`:grapix_status_api`](https://hex.pm/packages/grapix_status_api)  implements a self-contained GraphQL API module. Add it as a dependency to `my_api_project/mix.exs`:

```elixir
  def deps do
    [
      {:grapix_status_api, "~> 0.2"}
    ]
  end
```

Wire the new schema into `config/config.exs`:

```elixir

config :my_api_project, Schema, api_modules: [MyApiProjectApi, GrapixStatusApi]

```

Update your deps and then restart your server(`mix deps.get && mix phx.server`). When you refresh GraphiQL, you should see the new mutation and subscription in the documentation explorer.

## GraphQL Subscriptions

Javascript client-side support for GraphQL subscriptions is a bit of a
mess. Your Grapix API will support subscriptions out of the box, but
it can be tricky to get things working with your legacy clients. The
main complication is that Absinthe only supports clients that use the
Absinthe/Phoenix protocol, and this excludes legacy clients based on Apollo.

Here are some notes from my experiments with a few client-side stacks

### @absinthe/socket-apollo-link

[Straightforward client integraion](https://github.com/absinthe-graphql/absinthe-socket/tree/master/packages/socket-apollo-link#examples). Easiest route by far, and recommended for new clients. Configure the client to use the websocket endpoint mounted at `/graphql`

### Apollo client support *experimental*

New in version 0.2 is experimental support for the `graphql-ws` 
WebSockets sub-protocol, which you'll need for Apollo GraphQL-based
clients. Simply point your clients to the websocket endpoint mounted at `/graphql-ws` (_e.g._, `ws://localhost:4000/graphql-ws`).

_Warning: this support is definitely experimental_ I have not
   tested in any realistic settings. Consider it a proof-of-concept.

### MST-GQL Integration

[MST-GQL (MobX State Tree / GraphQL)](https://github.com/mobxjs/mst-gql) is a relatively new package, that does a *waaay* better data modeling job than Apollo. Even though it's still quite new, it's surprisingly robust.

Here's a [code snippet implementing an adaptor shim](https://gitlab.com/ekobi/grapix-framework/snippets/1954243) for MST-GQL that makes it compatible with the `@absinthe/socket-apollo-link` module.


## Pipeline
- automatically install all archives required by `:grapix_task`
- implement `--ecto` functionality in generator
- verify that multiple API modules using Ecto can peacefully coexist
- add support for testing subscriptions
- automate adding/dropping external API modules (`--include` generator functionality)
- implement `--api` generator functionality, to generate a standalone API project
- add a `:grapix_user_api` module
